from pathlib import Path

import PySimpleGUI as sg


project_dir = Path(__file__).resolve().parent

# GUI_THEME = "SystemDefaultForReal"
GUI_THEME = sg.OFFICIAL_PYSIMPLEGUI_THEME

DATA_DIR = project_dir / "data"
DB_DIR = project_dir / "db"
RESOURCES_DIR = project_dir / "resources"
DOCS_DIR = project_dir / "docs"
TEMPLATES_DIR = project_dir / "templates"

APP_NAME = (
    "Key Characteristics of Carcinogens: High Throughput Screening Discovery Tool"
)
APP_ORG = "World Health Organization: International Agency for Research on Cancer, Evidence Synthesis and Classification Branch"
APP_AUTHORS = "Brad Reisfeld"
APP_VERSION = "0.7.7"
HTS_DATA_SOURCE = "EPA ToxCast Database: invitrodb version 3.5"
KC_MAPPING_SOURCE = "IARC Monographs - Volume 123 (Annex 1: Section 4.4 spreadsheet) with changes by the IARC Secretariat for Volume 130 and updates for changes in the ToxCast/Tox21 data"

TITLE_IMAGE = RESOURCES_DIR / "kcs.png"
ICON_IMAGE = RESOURCES_DIR / "aflatoxin.png"

GUI_NORMAL_FONT = ("Arial", 11)
GUI_SUBTITLE_FONT = ("Arial", 12, "bold")
GUI_TITLE_FONT = ("Arial", 20, "bold")

DEFAULT_DATA_SOURCE = DB_DIR / "toxcast_data_20230929.pkl"
KEYCHAR_HTS_MAPPING = DATA_DIR / "keychar_toxcast_mapping_20211119.csv"
DEFAULT_ASSAY_INFO_SOURCE = DB_DIR / "assay_info_20230929.pkl"
HTS_KC_OUTPUT_TEMPLATE = TEMPLATES_DIR / "toxcast_data_template_20211119.xlsx"

DATA_FIELD_NAMES = [
    "CASRN",
    "DTXSID",
    "PREFERRED_NAME",
    "SMILES",
    "ASSAY_NAME",
    "ASSAY_ID",
    "HIT_CALL",
    "AC50",
    "FLAGS",
]

DB_TABLE_NAME = "toxcast_data"

ACTIVE_SYMBOL = "X"
INACTIVE_SYMBOL = ""
KC_MISSING_SYMBOL = "NA"
NAN_SYMBOL = "-"

KC_NUM_TO_NAME = {
    1: "01: Is electrophilic or can be metabolically activated to electrophiles",
    2: "02: Is genotoxic",
    3: "03: Alters DNA repair or causes genomic instability",
    4: "04: Induces epigenetic alterations",
    5: "05: Induces oxidative stress",
    6: "06: Induces chronic inflammation",
    7: "07: Is immunosuppressive",
    8: "08: Modulates receptor-mediated effects",
    9: "09: Causes immortalization",
    10: "10: Alters cell proliferation, cell death, or nutrient supply",
}

KC_NAME_TO_NUM = {v: k for k, v in KC_NUM_TO_NAME.items()}
